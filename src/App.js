import logo from './logo.svg';
import './App.css';
import Info from './components/info';

function App() {
  return (
    <div>
      <Info firstName="Minh" lastName=" Vu Dang" favNumber={25}></Info>
    </div>
  );
}

export default App;
